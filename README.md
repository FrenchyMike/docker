# DOCKER

Ce repo est consacré à la documentation et à la découverte de **[docker](https://www.docker.com/)**

## informations sur l'environnement:

* Ce repo est rédigé en avril 2020
* Virtualisation: Vitual Box
* La machine hôte
  * OS: CentOS 7
  * RAM: 1GB
  * CPU: 1
* Docker version: Docker version 1.13.1
* Docker-compose version: 
  * docker-compose version 1.25.3, build d4d1b42b
  * docker-py version: 4.1.0
  * CPython version: 3.7.5
  * OpenSSL version: OpenSSL 1.1.0l  10 Sep 2019

## [Wiki - Main page](https://gitlab.com/FrenchyMike/docker/-/wikis/home)
