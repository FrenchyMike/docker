#!/bin/bash

# Check presence of ssh key

[ -e $HOME/.ssh/id_rsa.pub ] && cp $HOME/.ssh/id_rsa.pub ./vm_key_rsa.pub; exit 0 || echo "no ssh key found, please run ssh-keygen"; exit 1